package info.hccis.performancehall_mobileappbasicactivity.entity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import info.hccis.performancehall_mobileappbasicactivity.R;
import info.hccis.performancehall_mobileappbasicactivity.ViewOrdersFragment;
import info.hccis.performancehall_mobileappbasicactivity.api.ApiWatcher;
import info.hccis.performancehall_mobileappbasicactivity.api.JsonTicketOrderApi;
import info.hccis.performancehall_mobileappbasicactivity.util.NotificationUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TicketOrderContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<TicketOrder> TICKET_ORDERS = new ArrayList<TicketOrder>();

    /**
     * Note that this class can provide an alternative way to load our list
     * to be used in the recycler view.  In the PHall app, we are using a ViewModel approach.
     * If we used this class, we would be having a static attribute which holds the
     * list of ticket orders.
     *
     * @author BJM
     * @since 20220203
     */

    public static void loadCustomers(Activity context) {

        Log.d("api BJM loadCustomers", "Accessing api at:" + ApiWatcher.API_BASE_URL);

        //Use Retrofit to connect to the service
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiWatcher.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonTicketOrderApi jsonTicketOrderApi = retrofit.create(JsonTicketOrderApi.class);

        //Create a list of customers.
        Call<List<TicketOrder>> call = jsonTicketOrderApi.getTicketOrders();

        //final reference to activity to get shared preferences
        final Activity contextIn = context;

        call.enqueue(new Callback<List<TicketOrder>>() {

            @Override
            public void onResponse(Call<List<TicketOrder>> call, Response<List<TicketOrder>> response) {

                List<TicketOrder> ticketOrders = new ArrayList();

                //**********************************************************************************
                // If not able to connect.
                //**********************************************************************************
                if (!response.isSuccessful()) {
                    Log.d("bjm api TicketOrderContent", "Unsuccessful response from rest: " + response.code());
                    SharedPreferences sharedPref = contextIn.getPreferences(Context.MODE_PRIVATE);
                    boolean preferToLoad = sharedPref.getBoolean(contextIn.getString(R.string.preference_load_from_room), false);

                    //******************************************************************************
                    //The following will use the database (to be implemented shortly in the course).
                    //******************************************************************************
//                    if (preferToLoad)
//                    {
//                        customers = getCustomersFromRoom();
//                        NotificationUtil.sendNotification("Customers loaded", "(" + customers.size() + ") Customers loaded from Room");
//                    } else {
//                        NotificationUtil.sendNotification("Customers not loaded", "Cannot load Customers - Connection Error");
//                        return;
//                    }


                } else {
                    //******************************************************************************
                    // We were able to connect.  Load the tickets orders
                    //***************************************************************************
                    ticketOrders = response.body();
                    NotificationUtil.sendNotification("Customers loaded", "(" + ticketOrders.size() + ") Customers loaded from API service");

                    //******************************************************************************
                    // Load to database
                    //***************************************************************************

                    //                    reloadCustomersInRoom(customers);

                }

                Log.d("BJM", "data back from service call #returned=" + ticketOrders.size());

                //**********************************************************************************
                // Now that we have the customers, will use them to assign values to the list which
                // is backing the recycler view.
                //**********************************************************************************

                TicketOrderContent.TICKET_ORDERS.clear();
                TicketOrderContent.TICKET_ORDERS.addAll(ticketOrders);

                //**********************************************************************************
                // The CustomerListFragment has a recyclerview which is used to show the customer list.
                // This recyclerview is backed by the customer list in the CustomerContent class.  After
                // this list is loaded, need to notify the adapter from the recyclerview that the
                // data is changed.
                //**********************************************************************************

                ViewOrdersFragment.getRecyclerView().getAdapter().notifyDataSetChanged();
                //Remove the progress bar from the view.

            }

            @Override
            public void onFailure(Call<List<TicketOrder>> call, Throwable t) {
                // If the api call failed, give a notification to the user.
                Log.d("bjm", "api call failed");
                Log.d("bjm", t.getMessage());

                SharedPreferences sharedPref = contextIn.getPreferences(Context.MODE_PRIVATE);
                boolean preferToLoad = sharedPref.getBoolean(contextIn.getString(R.string.preference_load_from_room), false);

                //Load from database if applicable based on user preference.

            }
        });
    }
}