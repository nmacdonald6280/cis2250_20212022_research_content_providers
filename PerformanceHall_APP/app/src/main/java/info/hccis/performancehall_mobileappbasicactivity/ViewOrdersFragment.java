package info.hccis.performancehall_mobileappbasicactivity;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import info.hccis.performancehall_mobileappbasicactivity.adapter.CustomAdapterTicketOrder;
import info.hccis.performancehall_mobileappbasicactivity.entity.TicketOrder;
import info.hccis.performancehall_mobileappbasicactivity.entity.TicketOrderViewModel;
import info.hccis.performancehall_mobileappbasicactivity.databinding.FragmentViewOrdersBinding;

public class ViewOrdersFragment extends Fragment {

    private FragmentViewOrdersBinding binding;
    private ArrayList<TicketOrder> ticketOrderArrayList;
    private static RecyclerView recyclerView;
    public static RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public static void notifyDataChanged(String message){
        Log.d("bjm", "Data changed:  "+message);
        //Send a notification that the data has changed.
        try {
            recyclerView.getAdapter().notifyDataSetChanged();
        }catch(Exception e){
            Log.d("bjm api","Exception when trying to notify that the data set as changed");
        }
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentViewOrdersBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //************************************************************************************
        // Corresponding to the add fragment, the view model is accessed to obtain a reference
        // to the list of ticket order bo objects.
        //************************************************************************************
        TicketOrderViewModel ticketOrderViewModel = new ViewModelProvider(getActivity()).get(TicketOrderViewModel.class);

        //************************************************************************************
        //NO LONGER USED BUT LEFT AS EXAMPLE
        //Bundle is accessed to get the ticket order which is passed from the add order fragment.
        //************************************************************************************
        //        Bundle bundle = getArguments(); //Note not doing anything...here for example
        //        TicketOrder ticketOrder = (TicketOrder) bundle.getSerializable(AddOrderFragment.KEY);
        //        Log.d("ViewOrdersFragment BJM", "Ticket passed in:  " + ticketOrder.toString());

        //************************************************************************************
        //Build the output to be displayed in the textview
        // NOTE:  This output string is no longer displayed since we added the RecyclerView
        //************************************************************************************

        String output = "";
        double total = 0;
        for (TicketOrder order : ticketOrderViewModel.getTicketOrders()) {
            output += order.toString() + System.lineSeparator();
            total += order.calculateTicketPrice();
        }
        output += System.lineSeparator() + "Total: $" + total;
        //binding.textviewOrderDetails.setText(output);  <--No longer displaying this output String

        //************************************************************************************
        // Setup the recycler view for displaying the items in the ticket order list.
        //************************************************************************************

        recyclerView = binding.recyclerView;

        ticketOrderArrayList = ticketOrderViewModel.getTicketOrders();
        setAdapter();

        //************************************************************************************
        // Button sends the user back to the add fragment
        //************************************************************************************

        binding.buttonAddOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(ViewOrdersFragment.this)
                        .navigate(R.id.action_ViewOrdersFragment_to_AddOrderFragment);
            }
        });
    }

    /**
     * Set the adapter for the recyclerview
     * @since 20220129
     * @author BJM
     */
    private void setAdapter() {
        CustomAdapterTicketOrder adapter = new CustomAdapterTicketOrder(ticketOrderArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}