package info.hccis.performancehall_mobileappbasicactivity.entity;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

public class TicketOrderViewModel extends ViewModel {

    private ArrayList<TicketOrder> ticketOrders = new ArrayList();

    public ArrayList<TicketOrder> getTicketOrders() {
        return ticketOrders;
    }

    public void setTicketOrders(ArrayList<TicketOrder> ticketOrders) {
        this.ticketOrders = ticketOrders;
    }
}
