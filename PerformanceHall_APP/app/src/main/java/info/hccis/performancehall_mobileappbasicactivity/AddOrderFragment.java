package info.hccis.performancehall_mobileappbasicactivity;

import static android.content.Intent.ACTION_INSERT;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import info.hccis.performancehall_mobileappbasicactivity.entity.TicketOrder;
import info.hccis.performancehall_mobileappbasicactivity.entity.TicketOrderViewModel;
import info.hccis.performancehall_mobileappbasicactivity.databinding.FragmentAddOrderBinding;

/**
 * AddOrderFragment
 * This Java class is associated with the add order fragment.  Note it works with the controls defined
 * on the res/layout/fragment_add_order layout file
 *
 * @author BJM/CIS2250
 * @since 20220129
 */

public class AddOrderFragment extends Fragment {

    Uri CONTENT_URI = Uri.parse("content://com.util.performancehall.provider/users");

    public static final String KEY = "info.hccis.performancehall.ORDER";
    private FragmentAddOrderBinding binding;
    private TicketOrder ticketOrder;
    private TicketOrderViewModel ticketOrderViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("AddOrderFragment BJM", "onCreate triggered");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d("AddOrderFragment BJM", "onCreateView triggered");
        binding = FragmentAddOrderBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * NOTE reviewing how to get permissions can be important.
     *
     * @param callbackId
     * @param permissionsId
     * @since 20220210
     */

    private void checkPermission(int callbackId, String... permissionsId) {
        boolean permissions = true;

        for (String p : permissionsId) {
            permissions = permissions && ContextCompat.checkSelfPermission(getContext(), p) == PERMISSION_GRANTED;
        }

        if (!permissions)
            ActivityCompat.requestPermissions(getActivity(), permissionsId, callbackId);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("AddOrderFragment BJM", "onViewCreated triggered");
        final int callbackId = 42;
        checkPermission(callbackId, Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR);

        ticketOrderViewModel = new ViewModelProvider(getActivity()).get(TicketOrderViewModel.class);

        //************************************************************************************
        // Add a listener on the button.
        //************************************************************************************

        binding.buttonCalculate.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("Range")
            @Override
            public void onClick(View view) {

                Log.d("AddOrderFragment BJM", "Calculate was clicked");

                try {
                    //************************************************************************************
                    // Call the calculate method
                    //************************************************************************************

                    calculate();

                    long eventID = createCalendarEvent();
                    Toast.makeText(getActivity(), "Calendar Event Created (" + eventID + ")", Toast.LENGTH_SHORT);
                    Log.d("BJM Calendar", "Calendar Event Created (" + eventID + ")");

                    //************************************************************************************
                    // I have made the TicketOrderBO implement Serializable.  This will allow us to
                    // serialize the object and then it can be passed in the bundle.  Note that the
                    // calculate method also sets the

                    Bundle bundle = new Bundle();
                    bundle.putSerializable(KEY, ticketOrder);

                    //************************************************************************************
                    // See the view model research component.  The ViewModel is associated with the Activity
                    // and the add and view fragments share the same activity.  Am using the view model
                    // to hold the list of TicketOrderBO objects.  This will allow both fragments to
                    // access the list.
                    //************************************************************************************

                    //TODO Handle the new ticket order
                    // don't want to add to the array list.  How should we handle this?
                    ticketOrderViewModel.getTicketOrders().add(ticketOrder);

                    //******************************************************************************
                    //Activity for Thursday.
                    //Call the post rest api web service to have the new ticket order added to the database.
                    //******************************************************************************

                    //************************************************************************************
                    // Navigate to the view orders fragment.  Note that the navigation is defined in
                    // the nav_graph.xml file.
                    //************************************************************************************
                    NavHostFragment.findNavController(AddOrderFragment.this)
                            .navigate(R.id.action_AddOrderFragment_to_ViewOrdersFragment, bundle);
                    //createCalendarEvent();
                    createCalendarEventIntent();
                } catch (Exception e) {
                    //************************************************************************************
                    // If there was an exception thrown in the calculate method, just put a message
                    // in the Logcat and leave the user on the add fragment.
                    //************************************************************************************
                    Log.d("AddOrderFragment BJM", "Error calculating: " + e.getMessage());
                }
            }
        });
    }

    /**
     * Add a calendar event based on the ticket order.
     *
     * @return event id
     * @author ANDROID_content_providers
     * @since 2022-02-10
     */

    public long createCalendarEvent() {
        //******************************************************************************
        // CREATE CALENDAR EVENT - start
        //******************************************************************************
        long calID = 1;  //   <------ default calendar
        long startMillis = 0;
        long endMillis = 0;
        Calendar beginTime = Calendar.getInstance();
        //Note month 0 based
        //beginTime.set(2022, 1, 10, 7, 30);
        startMillis = beginTime.getTimeInMillis(); //Using current time
        Calendar endTime = Calendar.getInstance();
        //endTime.set(2022, 1, 10, 8, 45);
        endMillis = endTime.getTimeInMillis() + 1000; //using current time plus a second

        ContentResolver cr = getActivity().getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, startMillis);
        values.put(CalendarContract.Events.DTEND, endMillis);
        values.put(CalendarContract.Events.TITLE, "Ticket Order Created");
        values.put(CalendarContract.Events.DESCRIPTION, ticketOrder.toString());
        values.put(CalendarContract.Events.CALENDAR_ID, calID);
        String[] zones = TimeZone.getAvailableIDs();

        values.put(CalendarContract.Events.EVENT_TIMEZONE, "America/Halifax");
        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

        // get the event ID that is the last element in the Uri
        long eventID = Long.parseLong(uri.getLastPathSegment());
        return eventID;
    }

    /**
     * Access calendar provider without permissions
     *
     * @author ANDROID_content_providers
     * @since 2022-02-10
     */
    public void createCalendarEventIntent() {
        //initialize Calendar intent
        Intent newIntent = new Intent(ACTION_INSERT);
        //set the intent datatype
        newIntent.setData(CalendarContract.Events.CONTENT_URI);
        //title of Calendar event
        newIntent.putExtra(CalendarContract.Events.TITLE, "Ticket order created");
        //send number of tickets to event description
        newIntent.putExtra(CalendarContract.Events.DESCRIPTION, ticketOrder.getNumberOfTickets() + " ticket order created");
        //make the event not "all day", so user can add start and end time in Calendar app
        newIntent.putExtra(CalendarContract.Events.ALL_DAY, false);
        //check to see if there is an app to support your action and start activity
        if (newIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(newIntent);
            //if there is not, display an appropriate message on the view tickets page
        } else {
            Toast.makeText(getActivity(), "There is no app that supports this action",
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * calculate the ticket cost based on the controls on the view.
     *
     * @throws Exception Throw exception if number of tickets entered caused an issue.
     * @author CIS2250
     * @since 20220118
     */
    public void calculate() throws Exception {

        Log.d("BJM-MainActivity", "HollPass Number entered =" + binding.editTextHollpassNumber.getText().toString());
        Log.d("BJM-MainActivity", "Number of tickets = " + binding.editTextNumberOfTickets.getText().toString());
        Log.d("BJM-MainActivity", "Calculate button was clicked.");

        int hollPassNumber = 0;
        try {
            hollPassNumber = Integer.parseInt(binding.editTextHollpassNumber.getText().toString());
        } catch (Exception e) {
            hollPassNumber = 0;
        }
        boolean validHollPassNumber;
        int numberOfTickets;
        try {
            numberOfTickets = Integer.parseInt(binding.editTextNumberOfTickets.getText().toString());
        } catch (Exception e) {
            numberOfTickets = 0;
        }
        ticketOrder = new TicketOrder();
        ticketOrder.setHollpassNumber(hollPassNumber);
        ticketOrder.setNumberOfTickets(numberOfTickets);

        //************************************************************************************
        // Add some validation to ensure that the ticket is in a correct range (1-20 in this case.
        //************************************************************************************

        try {
            if (!ticketOrder.validateNumberOfTickets()) {
                throw new Exception("Invalid Number of tickets");
            }
            double cost = ticketOrder.calculateTicketPrice();

            //************************************************************************************
            //Now that I have the cost, want to set the value on the textview.
            //************************************************************************************

            Locale locale = new Locale("en", "CA");
            DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(locale);
            DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(locale);
            decimalFormat.setDecimalFormatSymbols(dfs);
            String formattedCost = decimalFormat.format(cost);

            binding.textViewCost.setText(formattedCost);

        } catch (NumberFormatException nfe) {
            //************************************************************************************
            //This isn't possible as the edit text control is set to only accept numbers
            //************************************************************************************

            binding.editTextNumberOfTickets.setText("");
            binding.textViewCost.setText("Invalid number of tickets");
            binding.textViewCost.setError("Unable to calculate");
            throw nfe;
        } catch (Exception e) {
            //************************************************************************************
            // Exception is thrown if the number of tickets isn't in the valid range.
            //************************************************************************************
            binding.editTextNumberOfTickets.setText("");
            binding.textViewCost.setText("Maximum number of tickets is " + TicketOrder.MAX_TICKETS);
            binding.textViewCost.setError("Unable to calculate");
            throw e;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}